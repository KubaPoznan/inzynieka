import React, {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';
import './Navbar.css';
import {Button} from './Button';
function Navbar() {
    const[click, setClick] = useState(false);
    const [button, setButton] = useState(true);
    const handleClick = () => setClick(!click);
    const closeMobileMenu = () => setClick(false);
    
    const showButton = () => {
        if(window.innerWidth<=960){
            setButton(false)
        }
        else{
            setButton(true)
        }
    }
    useEffect(() => {
        showButton();
      }, []);
    window.addEventListener('resize', showButton);
    return (
    <div>
        <nav className='navbar'>
            <div className='navbar-container'>
                <Link to="/" className="navbar-logo" onClick={closeMobileMenu}>
                <i class="fas fa-clinic-medical"></i>JPWK Health
                </Link>
                <div className='menu-icon' onClick={handleClick}>
                    <i className={click ? 'fas fa-times' : 'fas fa-bars' } />
                
                </div> 
                <ul className={click ? 'nav-menu active' : 'nav-menu'}>
                <li className='nav-item'>
                    <Link to ='/' className='nav-links' onClick={closeMobileMenu} >
                        Strona Główna
                    </Link>
                </li>   
                <li className='nav-item'>
                    <Link to ='/oferta' className='nav-links' onClick={closeMobileMenu} >
                        Oferta
                    </Link>
                </li>   
                <li className='nav-item'>
                    <Link to ='/specjalizacje' className='nav-links' onClick={closeMobileMenu} >
                        Specjalizacje
                    </Link>
                </li>   
                <li className='nav-item'>
                    <Link to ='/aktualnosci' className='nav-links' onClick={closeMobileMenu} >
                        Aktualności 
                    </Link>
                </li>   
                <li className='nav-item'>
                    <Link to ='/kontakt' className='nav-links' onClick={closeMobileMenu} >
                        Kontakt 
                    </Link>
                </li>   
                <li>
                    <Link to ='/logowanie' className='nav-links-mobile' onClick={closeMobileMenu} >
                        Zaloguj się 
                    </Link>
                </li>   
                <li>
                    <Link to ='/logowanie' className='nav-links-mobile' onClick={closeMobileMenu} >
                        Zarejestruj się 
                    </Link>
                </li>   

                </ul>
                {button && <Button buttonStyle='btn--outline'>ZALOGUJ</Button>}
                {button && <Button buttonStyle='btn--outline'>ZAREJESTRUJ</Button>}
            </div>

        </nav>
    </div>
    )
}

export default Navbar
