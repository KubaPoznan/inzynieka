import React from 'react';
import loginimg from '../components/photos/loginimg.png';
import './Login.css';


  

function Login() {
    return (
        <div className='login-container'>
            
            <div className='login-bg'>
            <h1 className='login-h1'>Logowanie</h1>
            <div className='login-panel'>
                
                <div className='login-input'>
                <i class="far fa-user"></i>
                <input type='text' placeholder='Login'></input>
                </div>
                <div className='login-input'>
                <i class="fas fa-lock"></i>
                <input type='password' placeholder='Hasło'></input>
                </div>
                <div className='login-btn'>
                <input type='submit' value='Zaloguj' />
                </div>
                </div>
            </div>
            </div>
        
    )
}

export default Login
