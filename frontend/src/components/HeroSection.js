import React from 'react'
import {Button} from './Button'
import './HeroSection.css';
import doctorPhoto from '../components/photos/pqni.png';


function HeroSection() {
    return (
        <div className='hero-container'>
            <div className='hero-container-link'>
                <h1 className='hero-container-link-h1'>
                    W czym możemy Ci pomóc?
                </h1>    
                <p className ='hero-container-p'>Najlepsi specjaliściw zasięgu ręki, umów się <br/>
                na wizytę zdalną, bądź na miejscu już dziś!
                <br/> <Button buttonStyle='btn--primary'>Umów się</Button>
                </p>
            </div>    
            <div className='hero-container-img'>
                <img src={doctorPhoto} alt='Mrs. Doctor'/> 
                
            </div>
        
        </div>
    )
}

export default HeroSection
