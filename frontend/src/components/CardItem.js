import React from 'react'
import {Link} from 'react-router-dom';
function CardItem() {
    return (
        <div>
            <li className='cards__iten'>
                <Link to='/' className='cards__item__link'>
                    <figure className='cards__item__pic__wrap'>
                    
                    </figure>
                <div className='cards__item__info'>
                    <h5 className='cards__item__text'>RAZ</h5>
                </div>
                </Link>
            </li>
        </div>
    )
}

export default CardItem