import React from 'react';
import '../../App.css';
import Footer from '../Footer';
import HeroSection from '../HeroSection';
import Cards from '../Cards'
import Gallery from '.././/Gallery/Gallery';

function home(){
return(
<div>
<HeroSection />
<Cards />
<Gallery />
<Footer />
</div>


)}
export default home;
