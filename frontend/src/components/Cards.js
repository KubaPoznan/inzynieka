import React from 'react';
import './Cards.css';
import CardItem from './CardItem'
function Cards() {
    return (
        <div className='card-container'>
            <p className='card-container-h'>Poznaj nasz serwis</p>
                <p className='card-container-p'>Oferujemy kompleksową usługę umożliwiającą nieograniczony kontakt
                <br/>ze specjalistami, nieograniczony dostęp do badań, jeżeli masz problem, 
                <br/>niezależnie od pory dnia, nasi specjaliści chętnie pomogą.
                </p>
            <div className='card-links'>
            <div className='card-link-wrapper'>
                <div className='card-link-items'>
                <i class="fas fa-vials"></i>
                <h4>  Dostęp do badań <br/> laboratoryjnych</h4>
                Uzyskaj szybki i łatwy dostęp do <br />swoich badań laboratoryjnych
                </div>
                <div className='card-link-items'>
                <i class="fas fa-vials"></i>
                <h4>  Dostęp do badań <br/> laboratoryjnych</h4>
                Uzyskaj szybki i łatwy dostęp do <br />swoich badań laboratoryjnych
                </div><div className='card-link-items'>
                <i class="fas fa-vials"></i>
                <h4>  Dostęp do badań <br/> laboratoryjnych</h4>
                Uzyskaj szybki i łatwy dostęp do <br />swoich badań laboratoryjnych
                </div><div className='card-link-items'>
                <i class="fas fa-vials"></i>
                <h4>  Dostęp do badań <br/> laboratoryjnych</h4>
                Uzyskaj szybki i łatwy dostęp do <br />swoich badań laboratoryjnych
                </div><div className='card-link-items'>
                <i class="fas fa-vials"></i>
                <h4>  Dostęp do badań <br/> laboratoryjnych</h4>
                Uzyskaj szybki i łatwy dostęp do <br />swoich badań laboratoryjnych
                </div><div className='card-link-items'>
                <i class="fas fa-vials"></i>
                <h4>  Dostęp do badań <br/> laboratoryjnych</h4>
                Uzyskaj szybki i łatwy dostęp do <br />swoich badań laboratoryjnych
                </div>

            </div>

            </div>
            
        </div>
    )
}

export default Cards
