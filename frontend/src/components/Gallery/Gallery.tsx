import { FunctionComponent} from 'react';
import { images } from '../../resources/gallery-data';
import './Gallery.css';
import ImageGallery from 'react-image-gallery';
import "react-image-gallery/styles/css/image-gallery.css";

export const Gallery: FunctionComponent<any> = () => {


    return (
        <div>
            <ImageGallery items={images} autoPlay={true}></ImageGallery>
        </div>
    )
}



export default Gallery;