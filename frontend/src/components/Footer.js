import React from 'react'
import { Link } from 'react-router-dom';
import './Footer.css'
function Footer() {
    return (
        <div className='footer-container'>
           <div className='footer-links'>
               <div className='footer-link-wrapper'>
                   <div className='footer-link-items'>
                      <h2>Zdjęcie</h2>
                       <Link to='/'>Głogowska 3, 60-104 Poznań</Link> 
                       <Link to='/'>123456789</Link> 
                       <Link to='/'>jpwkhealth@jpwkheatlh.com</Link> 
                   </div>
                   <div className='footer-link-items'>
                      <h2>Pomoc</h2>
                       <Link to='/'>Polityka prywatbności</Link> 
                       <Link to='/'>Polityka cookie</Link> 
                       <Link to='/'>Polityka płatności</Link> 
                   </div>
                   <div className='footer-link-items'>
                      <h2>Pomoc</h2>
                       <Link to='/'>Polityka prywatbności</Link> 
                       <Link to='/'>Polityka cookie</Link> 
                       <Link to='/'>Polityka płatności</Link> 
                   </div>
                   <div className='footer-link-items'>
                      <h2>Menu</h2>
                       <Link to='/'>Strona główna</Link> 
                       <Link to='/'>Oferta</Link> 
                       <Link to='/'>Specjalizacje</Link> 
                       <Link to='/'>Kontakt</Link> 
                       <Link to='/'>Aktualności</Link> 
                       <Link to='/'>Logowanie</Link> 
                       <Link to='/'>Rejestracja</Link> 
                   </div>
                   <div className='footer-link-items'>
                      <h2>Kontakt</h2>
                       <Link to='/'>Prasa</Link> 
                       <Link to='/'>Pomoc techniczna</Link> 
                       
                   </div>
               </div>
           </div>
        </div>
    )
}

export default Footer
       