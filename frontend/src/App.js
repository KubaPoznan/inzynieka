import React from 'react';
import Navbar from './components/Navbar';
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import Home from './components/pages/home.js';

function App() {
  
  
  return (
    // <div>
    //   <div className="header"><Header /></div>
    //   <div> <MainNavi/> </div>
    //   <Gallery/>
    //   <div> <Footer/>
    //   </div>

    // </div>
    <div className="App">
      <Router>
      <Navbar />
      <Routes>
        
        <Route path="/" element={<Home/>} />
      </Routes>



      </Router>
    </div>
  );
}

export default App;
