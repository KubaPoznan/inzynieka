package pl.wsb.inzynierka.backend.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Optional;

@RepositoryRestResource
interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByEmailAndPassword (String email, String password);
}
