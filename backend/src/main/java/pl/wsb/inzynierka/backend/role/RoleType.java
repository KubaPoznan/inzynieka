package pl.wsb.inzynierka.backend.role;

public enum RoleType {
    PATIENT,
    DOCTOR,
    REGISTRATOR;
}
