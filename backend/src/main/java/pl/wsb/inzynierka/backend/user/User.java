package pl.wsb.inzynierka.backend.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.experimental.Accessors;
import pl.wsb.inzynierka.backend.exam.Exam;
import pl.wsb.inzynierka.backend.message.Message;
import pl.wsb.inzynierka.backend.role.Role;
import pl.wsb.inzynierka.backend.specialisation.Specialisation;
import pl.wsb.inzynierka.backend.visit.Visit;

import javax.persistence.*;
import java.util.List;
@Getter
@Setter
@Accessors(chain = true)

@Entity
@NoArgsConstructor

public class User {
    @Id
    @GeneratedValue
    private Long id;

    private String firstName;

    private String lastName;

    private String peselNumber;

    private String email;
    @JsonProperty
    private String password;



    @JsonIgnore
    public String getPassword() {
        return password;
    }


    @OneToMany(mappedBy = "doctor")
    private List<Visit> visitDoctor;
    @OneToMany(mappedBy = "patient")
    private List<Visit> visitPatient;
    @OneToMany(mappedBy = "doctor")
    private List<Exam> examDoctor;
    @OneToMany(mappedBy = "patient")
    private List<Exam> examPatient;
    @ManyToMany(mappedBy = "user")
    private List<Role> role;
    @ManyToMany(mappedBy = "user")
    private List<Specialisation> specialisation;
    @OneToMany(mappedBy = "sender")
    private List<Message> messageSender;
    @OneToMany(mappedBy = "receiver")
    private List<Message> messageReceiver;


   /* @JsonIgnore
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }*/
}
