package pl.wsb.inzynierka.backend.specialisation;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.wsb.inzynierka.backend.user.User;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
public class Specialisation {
    @Id
    @GeneratedValue
    private Long id;
    private SpecialisationName specialisationName;
    @ManyToMany
    private List<User> user;
}
