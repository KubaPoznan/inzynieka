package pl.wsb.inzynierka.backend.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import pl.wsb.inzynierka.backend.exam.Exam;
import pl.wsb.inzynierka.backend.facility.Facility;
import pl.wsb.inzynierka.backend.message.Message;
import pl.wsb.inzynierka.backend.office.Office;
import pl.wsb.inzynierka.backend.role.Role;
import pl.wsb.inzynierka.backend.specialisation.Specialisation;
import pl.wsb.inzynierka.backend.user.User;
import pl.wsb.inzynierka.backend.visit.Visit;

@Configuration
class RestResourceConfiguration implements RepositoryRestConfigurer {

    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config, CorsRegistry cors) {
        config.exposeIdsFor(User.class, Visit.class, Facility.class, Exam.class, Role.class, Office.class, Specialisation.class, Message.class);
    }

}
