package pl.wsb.inzynierka.backend.facility;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "facilities")
interface FacilityRepository extends JpaRepository<Facility, Long> {
}