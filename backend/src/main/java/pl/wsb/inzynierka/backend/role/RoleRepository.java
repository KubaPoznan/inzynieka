package pl.wsb.inzynierka.backend.role;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
interface RoleRepository extends JpaRepository<Role, Long> {
}