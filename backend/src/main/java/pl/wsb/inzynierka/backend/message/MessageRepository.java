package pl.wsb.inzynierka.backend.message;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
interface MessageRepository extends JpaRepository<Message, Long> {
}