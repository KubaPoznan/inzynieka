package pl.wsb.inzynierka.backend.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
public class UserControler {
    @Autowired
    UserRepository userRepository;


    @GetMapping(path = "/userlogin")
    public ResponseEntity<User> returnUserIfExist(@RequestParam String email, @RequestParam String password) {
        return ResponseEntity.ok()
                .body(this.userRepository.findByEmailAndPassword(email, password)
                        .orElseThrow(UserNotFoundException::new));
                
    }
}
