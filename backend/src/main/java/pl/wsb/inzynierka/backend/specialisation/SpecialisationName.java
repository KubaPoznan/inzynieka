package pl.wsb.inzynierka.backend.specialisation;

public enum SpecialisationName {
    GINEKOLOG,
    CHIRURG,
    PEDIATRA,
    UROLOG,
    NEUROLOG,
    ORTOPEDA,
    DENTYSTA,
    LARYNGOLOG,
    GASTROLOG,
    LEKARZ_RODZINNY;
}