package pl.wsb.inzynierka.backend.exam;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
interface ExamRepository extends JpaRepository<Exam, Long> {
}