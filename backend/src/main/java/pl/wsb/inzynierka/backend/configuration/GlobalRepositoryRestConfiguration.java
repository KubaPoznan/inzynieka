package pl.wsb.inzynierka.backend.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
import org.springframework.web.servlet.config.annotation.CorsRegistry;

@Configuration
class GlobalRepositoryRestConfiguration implements RepositoryRestConfigurer {

    @Autowired
    FrontendConfiguration frontendConfiguration;
    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config, CorsRegistry cors) {
        cors.addMapping("/**")
        .allowedOrigins(frontendConfiguration.getUrl());
    }
}
