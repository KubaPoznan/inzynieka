package pl.wsb.inzynierka.backend.visit;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum Status {
    WAITING,
    ACCEPTED,
    REJECTED;
}
