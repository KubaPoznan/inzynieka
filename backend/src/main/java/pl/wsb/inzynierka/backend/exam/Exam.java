package pl.wsb.inzynierka.backend.exam;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.wsb.inzynierka.backend.facility.Facility;
import pl.wsb.inzynierka.backend.user.User;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Exam {
    @Id
    @GeneratedValue
    private Long id;

    private String name;

    private LocalDateTime date;
    @ManyToOne
    private Facility facility;
    @ManyToOne
    private User doctor;
    @ManyToOne
    private User patient;
}

