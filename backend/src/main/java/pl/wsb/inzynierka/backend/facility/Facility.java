package pl.wsb.inzynierka.backend.facility;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.wsb.inzynierka.backend.exam.Exam;
import pl.wsb.inzynierka.backend.office.Office;
import pl.wsb.inzynierka.backend.visit.Visit;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Facility {
    @Id
    @GeneratedValue
    private Long id;

    private String city;

    private String street;

    private Integer buildingNumber;

    private Integer localNumber;

    @OneToMany(mappedBy = "")
    private List<Visit> visits;

    @OneToMany(mappedBy = "facility")
    private List<Exam> exam;

    @OneToMany(mappedBy = "facility")
    private List<Office> offices;

}