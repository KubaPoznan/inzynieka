package pl.wsb.inzynierka.backend.visit;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
interface VisitRepository extends JpaRepository<Visit, Long> {
}
