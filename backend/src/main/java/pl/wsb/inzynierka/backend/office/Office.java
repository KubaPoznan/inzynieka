package pl.wsb.inzynierka.backend.office;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.wsb.inzynierka.backend.facility.Facility;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Office {
    @Id
    @GeneratedValue
    private Long id;

    private Integer number;

    private Integer localNumber;

    @ManyToOne
    private Facility facility;


}