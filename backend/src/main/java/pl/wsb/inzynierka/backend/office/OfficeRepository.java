package pl.wsb.inzynierka.backend.office;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
interface OfficeRepository extends JpaRepository<Office, Long> {
}