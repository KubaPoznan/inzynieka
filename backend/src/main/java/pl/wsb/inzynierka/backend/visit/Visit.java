package pl.wsb.inzynierka.backend.visit;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.wsb.inzynierka.backend.facility.Facility;
import pl.wsb.inzynierka.backend.user.User;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Visit {
    @Id
    @GeneratedValue
    private Long id;

    private Status status;

    private LocalDateTime date;
    @ManyToOne
    private User doctor;
    @ManyToOne
    private User patient;
    @ManyToOne
    private Facility facility;



}
