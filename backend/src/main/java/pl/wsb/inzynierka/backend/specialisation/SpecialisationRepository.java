package pl.wsb.inzynierka.backend.specialisation;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
interface SpecialisationRepository extends JpaRepository<Specialisation, Long> {
}