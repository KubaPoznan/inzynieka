package pl.wsb.inzynierka.backend.office;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import pl.wsb.inzynierka.backend.exam.Exam;
import pl.wsb.inzynierka.backend.facility.Facility;
import pl.wsb.inzynierka.backend.utils.EntityUtil;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;
import static pl.wsb.inzynierka.backend.utils.HttpUtil.getHttpEntity;
import static pl.wsb.inzynierka.backend.utils.LinkUtil.FACILITIES;
import static pl.wsb.inzynierka.backend.utils.LinkUtil.OFFICES;


@SpringBootTest(webEnvironment = DEFINED_PORT)
class OfficeTest {

    TestRestTemplate testRestTemplate = new TestRestTemplate();

    @Test
    void officeIsAdded_retrievedOfficeInformation_dataIsOk() {
        //given
        Office savedOffice = testRestTemplate
                .postForEntity(OFFICES, EntityUtil.office, Office.class).getBody();

        Facility savedFacility = testRestTemplate
                .postForEntity(FACILITIES, EntityUtil.facility, Facility.class).getBody();

        //when
        Assertions.assertNotNull(savedOffice);
        Assertions.assertNotNull(savedFacility);

        testRestTemplate.put(OFFICES + "/" + savedOffice.getId() + "/facility", getHttpEntity(FACILITIES + "/" + savedFacility.getId()));

        Office retrievedOffice = testRestTemplate
                .getForEntity(OFFICES + "/" + savedOffice.getId(), Office.class).getBody();

        Facility retrievedFacility = testRestTemplate
                .getForEntity(OFFICES + "/" + savedOffice.getId() + "/facility", Facility.class).getBody();

        //then
        Assertions.assertNotNull(retrievedOffice);
        assertThat(savedFacility).isEqualTo(retrievedFacility);
        assertThat(savedOffice).isEqualTo(retrievedOffice);
    }

    @Test
    void officeIsDeleted_retrievedOfficeInformation_noData() {
        //given
        Office savedOffice = testRestTemplate
                .postForEntity(OFFICES, EntityUtil.office, Office.class).getBody();

        //when
        Assertions.assertNotNull(savedOffice);
        testRestTemplate.delete(OFFICES + "/" + savedOffice.getId());
        Exam retrievedOffice = testRestTemplate
                .getForEntity(OFFICES + "/" + savedOffice.getId(), Exam.class).getBody();

        //then
        Assertions.assertNull(retrievedOffice);
    }

    @Test
    void officeIsUpdated_retrievedOfficeInformation_dataOk() {
        //given
        Office savedOffice = testRestTemplate
                .postForEntity(OFFICES, EntityUtil.office, Office.class).getBody();

        //when
        Assertions.assertNotNull(savedOffice);
        savedOffice.setNumber(2);
        testRestTemplate.put(OFFICES + "/" + savedOffice.getId(), savedOffice);
        Office retrievedOffice = testRestTemplate
                .getForEntity(OFFICES + "/" + savedOffice.getId(), Office.class).getBody();

        //then
        Assertions.assertNotNull(retrievedOffice);
        Assertions.assertEquals(savedOffice, retrievedOffice);
    }
}
