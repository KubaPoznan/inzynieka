package pl.wsb.inzynierka.backend.visit;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import pl.wsb.inzynierka.backend.user.User;
import pl.wsb.inzynierka.backend.utils.EntityUtil;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;
import static pl.wsb.inzynierka.backend.utils.HttpUtil.getHttpEntity;
import static pl.wsb.inzynierka.backend.utils.LinkUtil.USERS;
import static pl.wsb.inzynierka.backend.utils.LinkUtil.VISITS;

@SpringBootTest(webEnvironment = DEFINED_PORT)
class VisitTest {

    TestRestTemplate testRestTemplate = new TestRestTemplate();

    @Test
    void visitIsAdded_receiveVisitInformation_dataIsOk() {
        //given
        User savedUser = testRestTemplate.postForEntity(USERS, EntityUtil.user, User.class).getBody();
        Visit saveVisit = testRestTemplate.postForEntity(VISITS, EntityUtil.visit, Visit.class).getBody();

        //when
        Assertions.assertNotNull(savedUser);
        Assertions.assertNotNull(saveVisit);

        testRestTemplate.put(VISITS + "/" + saveVisit.getId() + "/doctor", getHttpEntity(USERS + "/" + savedUser.getId()));
        testRestTemplate.put(VISITS + "/" + saveVisit.getId() + "/patient", getHttpEntity(USERS + "/" + savedUser.getId()));

        User retrieveDoctor = testRestTemplate.getForEntity(VISITS + "/" + saveVisit.getId() + "/doctor", User.class).getBody();
        User retrievePatient = testRestTemplate.getForEntity(VISITS + "/" + saveVisit.getId() + "/patient", User.class).getBody();
        Visit retrieveVisit = testRestTemplate.getForEntity(VISITS + "/" + saveVisit.getId(), Visit.class).getBody();

        //then
        Assertions.assertEquals(savedUser, retrieveDoctor);
        Assertions.assertEquals(savedUser, retrievePatient);
        Assertions.assertNotNull(retrieveVisit);
        assertThat(saveVisit.getDate()).isEqualToIgnoringNanos(retrieveVisit.getDate());
        assertThat(saveVisit).usingRecursiveComparison().ignoringFields("date").isEqualTo(retrieveVisit);
    }

    @Test
    void visitIsDeleted_receiveVisitInformation_noData() {
        //given
        Visit savedVisit = testRestTemplate.postForEntity(VISITS, EntityUtil.visit, Visit.class).getBody();

        //when
        Assertions.assertNotNull(savedVisit);
        testRestTemplate.delete(VISITS + "/" + savedVisit.getId());
        Visit retrievedVisit = testRestTemplate.getForEntity(VISITS + "/" + savedVisit.getId(), Visit.class).getBody();

        //then
        Assertions.assertNull(retrievedVisit);
    }

    @Test
    void visitIsUpdated_receiveVisitInformation_dataOk() {
        //given

        Visit savedVisit = testRestTemplate.postForEntity(VISITS, EntityUtil.visit, Visit.class).getBody();

        //when
        Assertions.assertNotNull(savedVisit);
        savedVisit.setStatus(Status.REJECTED);
        testRestTemplate.put(VISITS + "/" + savedVisit.getId(), savedVisit);
        Visit retrievedVisit = testRestTemplate.getForEntity(VISITS + "/" + savedVisit.getId(), Visit.class).getBody();

        //then
        Assertions.assertNotNull(retrievedVisit);
        assertThat(savedVisit.getDate()).isEqualToIgnoringNanos(retrievedVisit.getDate());
        assertThat(savedVisit).usingRecursiveComparison().ignoringFields("date").isEqualTo(retrievedVisit);
    }
}
