package pl.wsb.inzynierka.backend.utils;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.wsb.inzynierka.backend.user.User;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmbeddedUser {
    @JsonProperty("_embedded")
    private Embedded embedded;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public class Embedded {

        private List<User> users;
    }
}
