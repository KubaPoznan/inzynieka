package pl.wsb.inzynierka.backend.utils;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

public class HttpUtil {

    public static HttpEntity<String> getHttpEntity(String url) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(new MediaType("text", "uri-list"));

        return new HttpEntity<>(url, headers);
    }
}
