package pl.wsb.inzynierka.backend.utils;

import lombok.experimental.UtilityClass;

import java.time.LocalDateTime;

@UtilityClass
public class TimeUtil {
    public static final LocalDateTime now = LocalDateTime.now();
}
