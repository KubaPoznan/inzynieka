package pl.wsb.inzynierka.backend.message;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import pl.wsb.inzynierka.backend.user.User;
import pl.wsb.inzynierka.backend.utils.EntityUtil;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;
import static pl.wsb.inzynierka.backend.utils.HttpUtil.getHttpEntity;
import static pl.wsb.inzynierka.backend.utils.LinkUtil.MESSAGES;
import static pl.wsb.inzynierka.backend.utils.LinkUtil.USERS;

@SpringBootTest(webEnvironment = DEFINED_PORT)
class MessageTest {
    TestRestTemplate testRestTemplate = new TestRestTemplate();

    @Test
    void messageIsAdded_receiveMessageInformation_dataIsOk() {
        //given
        User savedUser = testRestTemplate.postForEntity(USERS, EntityUtil.user, User.class).getBody();
        Message saveMessage = testRestTemplate.postForEntity(MESSAGES, EntityUtil.message, Message.class).getBody();

        //when
        Assertions.assertNotNull(savedUser);
        Assertions.assertNotNull(saveMessage);

        testRestTemplate.put(MESSAGES + "/" + saveMessage.getId() + "/sender", getHttpEntity(USERS + "/" + savedUser.getId()));
        testRestTemplate.put(MESSAGES + "/" + saveMessage.getId() + "/receiver", getHttpEntity(USERS + "/" + savedUser.getId()));


        User retrieveSender = testRestTemplate.getForEntity(MESSAGES + "/" + saveMessage.getId() + "/sender", User.class).getBody();
        User retrieveReceiver = testRestTemplate.getForEntity(MESSAGES + "/" + saveMessage.getId() + "/receiver", User.class).getBody();
        Message retrieveMessage = testRestTemplate.getForEntity(MESSAGES + "/" + saveMessage.getId(), Message.class).getBody();

        //then
        Assertions.assertEquals(savedUser, retrieveSender);
        Assertions.assertEquals(savedUser, retrieveReceiver);
        Assertions.assertNotNull(retrieveMessage);
        assertThat(saveMessage.getDate()).isEqualToIgnoringNanos(retrieveMessage.getDate());
        assertThat(saveMessage).usingRecursiveComparison().ignoringFields("date").isEqualTo(retrieveMessage);
    }

    @Test
    void messageIsDeleted_receiveMessageInformation_noData() {
        //given
        Message saveMessage = testRestTemplate.postForEntity(MESSAGES, EntityUtil.message, Message.class).getBody();

        //when
        Assertions.assertNotNull(saveMessage);
        testRestTemplate.delete(MESSAGES + "/" + saveMessage.getId());
        Message retrievedMessage = testRestTemplate.getForEntity(MESSAGES + "/" + saveMessage.getId(), Message.class).getBody();

        //then
        Assertions.assertNull(retrievedMessage);
    }

    @Test
    void messageIsUpdated_receiveMessageInformation_dataOk() {
        //given

        Message saveMessage = testRestTemplate.postForEntity(MESSAGES, EntityUtil.message, Message.class).getBody();

        //when
        Assertions.assertNotNull(saveMessage);
        saveMessage.setTitle("Odpowiedź");
        testRestTemplate.put(MESSAGES + "/" + saveMessage.getId(), saveMessage);
        Message retrievedMessage = testRestTemplate.getForEntity(MESSAGES + "/" + saveMessage.getId(), Message.class).getBody();

        //then
        Assertions.assertNotNull(retrievedMessage);
        assertThat(saveMessage.getDate()).isEqualToIgnoringNanos(retrievedMessage.getDate());
        assertThat(saveMessage).usingRecursiveComparison().ignoringFields("date").isEqualTo(retrievedMessage);
    }
}
