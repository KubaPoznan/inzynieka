package pl.wsb.inzynierka.backend.role;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import pl.wsb.inzynierka.backend.user.User;
import pl.wsb.inzynierka.backend.utils.EmbeddedUser;
import pl.wsb.inzynierka.backend.utils.EntityUtil;

import java.util.List;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;
import static pl.wsb.inzynierka.backend.utils.HttpUtil.getHttpEntity;
import static pl.wsb.inzynierka.backend.utils.LinkUtil.ROLES;
import static pl.wsb.inzynierka.backend.utils.LinkUtil.USERS;

@SpringBootTest(webEnvironment = DEFINED_PORT)
class RoleTest {
    TestRestTemplate testRestTemplate = new TestRestTemplate();

    @Test
    void roleIsAdded_receiveRoleInformation_dataIsOk() {
        //given
        User savedUser = testRestTemplate.postForEntity(USERS, EntityUtil.user, User.class).getBody();
        Role saveRole = testRestTemplate.postForEntity(ROLES, EntityUtil.role, Role.class).getBody();

        //when
        Assertions.assertNotNull(savedUser);
        Assertions.assertNotNull(saveRole);

        testRestTemplate.put(ROLES + "/" + saveRole.getId() + "/user", getHttpEntity(USERS + "/" + savedUser.getId()));

        EmbeddedUser retrieveUser = testRestTemplate.getForEntity(ROLES + "/" + saveRole.getId() + "/user", EmbeddedUser.class).getBody();
        Role retrieveRole = testRestTemplate.getForEntity(ROLES + "/" + saveRole.getId(), Role.class).getBody();
        //then
        Assertions.assertNotNull(retrieveUser);
        Assertions.assertNotNull(retrieveUser.getEmbedded());
        Assertions.assertEquals(List.of(savedUser), retrieveUser.getEmbedded().getUsers());
        Assertions.assertEquals(saveRole, retrieveRole);
    }

    @Test
    void roleIsDeleted_receiveRoleInformation_noData() {
        //given
        Role saveRole = testRestTemplate.postForEntity(ROLES, EntityUtil.role, Role.class).getBody();

        //when
        Assertions.assertNotNull(saveRole);
        testRestTemplate.delete(ROLES + "/" + saveRole.getId());
        Role retrievedRole = testRestTemplate.getForEntity(ROLES + "/" + saveRole.getId(), Role.class).getBody();

        //then
        Assertions.assertNull(retrievedRole);
    }

    @Test
    void roleIsUpdated_receiveRoleInformation_dataOk() {
        //given
        Role saveRole = testRestTemplate.postForEntity(ROLES, EntityUtil.role, Role.class).getBody();

        //when
        Assertions.assertNotNull(saveRole);
        saveRole.setRoleType(RoleType.DOCTOR);
        testRestTemplate.put(ROLES + "/" + saveRole.getId(), saveRole);
        Role retrievedRole = testRestTemplate.getForEntity(ROLES + "/" + saveRole.getId(), Role.class).getBody();

        //then
        Assertions.assertEquals(saveRole, retrievedRole);
    }

}
