package pl.wsb.inzynierka.backend.facility;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import pl.wsb.inzynierka.backend.exam.Exam;
import pl.wsb.inzynierka.backend.utils.EntityUtil;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;
import static pl.wsb.inzynierka.backend.utils.HttpUtil.getHttpEntity;
import static pl.wsb.inzynierka.backend.utils.LinkUtil.EXAMS;
import static pl.wsb.inzynierka.backend.utils.LinkUtil.FACILITIES;


@SpringBootTest(webEnvironment = DEFINED_PORT)
class FacilityTest {

    TestRestTemplate testRestTemplate = new TestRestTemplate();

    @Test
    void facilityIsAdded_retrievedFacilityInformation_dataIsOk() {
        //given
        Facility savedFacility = testRestTemplate
                .postForEntity(FACILITIES, EntityUtil.facility, Facility.class).getBody();
        Exam savedExam = testRestTemplate
                .postForEntity(EXAMS, EntityUtil.exam, Exam.class).getBody();

        //when
        Assertions.assertNotNull(savedFacility);
        Assertions.assertNotNull(savedExam);

        testRestTemplate.put(FACILITIES + "/" + savedFacility.getId() + "/exam", getHttpEntity(EXAMS + "/" + savedExam.getId()));

        Exam retrievedExam = testRestTemplate.getForEntity(FACILITIES + "/" + savedFacility.getId() + "/exam", Exam.class).getBody();
        Facility retrievedFacility = testRestTemplate.getForEntity(FACILITIES + "/" + savedFacility.getId(), Facility.class).getBody();
        //then
        Assertions.assertNotNull(retrievedExam);
        assertThat(savedExam.getDate()).isEqualToIgnoringNanos(retrievedExam.getDate());
        assertThat(savedExam).usingRecursiveComparison().ignoringFields("date").isEqualTo(retrievedExam);
        Assertions.assertEquals(savedFacility, retrievedFacility);

    }

    @Test
    void facilityIsDeleted_retrievedFacilityInformation_noData() {
        //given
        Facility savedFacility = testRestTemplate
                .postForEntity(FACILITIES, EntityUtil.facility, Facility.class).getBody();

        //when
        Assertions.assertNotNull(savedFacility);
        testRestTemplate.delete(FACILITIES + "/" + savedFacility.getId());
        Facility retrievedFacility = testRestTemplate
                .getForEntity(FACILITIES + "/" + savedFacility.getId(), Facility.class).getBody();

        //then
        Assertions.assertNull(retrievedFacility);

    }

    @Test
    void facilityIsUpdated_retrievedFacilityInformation_dataOk() {
        //given
        Facility savedFacility = testRestTemplate
                .postForEntity(FACILITIES, EntityUtil.facility, Facility.class).getBody();

        //when
        Assertions.assertNotNull(savedFacility);
        savedFacility.setCity("Szczecin");
        testRestTemplate.put(FACILITIES + "/" + savedFacility.getId(), savedFacility);
        Facility retrievedFacility = testRestTemplate
                .getForEntity(FACILITIES + "/" + savedFacility.getId(), Facility.class).getBody();

        //then
        Assertions.assertEquals(savedFacility, retrievedFacility);
    }
}
