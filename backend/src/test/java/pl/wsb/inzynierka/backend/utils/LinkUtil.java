package pl.wsb.inzynierka.backend.utils;

import lombok.experimental.UtilityClass;

@UtilityClass
public class LinkUtil {
    public static final String USERS = "http://localhost:8080/users";
    public static final String VISITS = "http://localhost:8080/visits";
    public static final String FACILITIES = "http://localhost:8080/facilities";
    public static final String EXAMS = "http://localhost:8080/exams";
    public static final String ROLES = "http://localhost:8080/roles";
    public static final String OFFICES = "http://localhost:8080/offices";
    public static final String SPECIALISATIONS = "http://localhost:8080/specialisations";
    public static final String MESSAGES = "http://localhost:8080/messages";
    public static final String USERLOGIN = "http://localhost:8080/userlogin";

}
