package pl.wsb.inzynierka.backend.specialisation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import pl.wsb.inzynierka.backend.user.User;
import pl.wsb.inzynierka.backend.utils.EmbeddedUser;
import pl.wsb.inzynierka.backend.utils.EntityUtil;

import java.util.List;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;
import static pl.wsb.inzynierka.backend.utils.HttpUtil.getHttpEntity;
import static pl.wsb.inzynierka.backend.utils.LinkUtil.SPECIALISATIONS;
import static pl.wsb.inzynierka.backend.utils.LinkUtil.USERS;

@SpringBootTest(webEnvironment = DEFINED_PORT)
class SpecialisationTest {
    TestRestTemplate testRestTemplate = new TestRestTemplate();

    @Test
    void specialisationIsAdded_receiveSpecialisationInformation_dataIsOk() {
        //given
        User savedUser = testRestTemplate.postForEntity(USERS, EntityUtil.user, User.class).getBody();
        Specialisation saveSpecialisation = testRestTemplate.postForEntity(SPECIALISATIONS, EntityUtil.specialisation, Specialisation.class).getBody();

        //when
        Assertions.assertNotNull(savedUser);
        Assertions.assertNotNull(saveSpecialisation);

        testRestTemplate.put(SPECIALISATIONS + "/" + saveSpecialisation.getId() + "/user", getHttpEntity(USERS + "/" + savedUser.getId()));

        EmbeddedUser retrieveUser = testRestTemplate.getForEntity(SPECIALISATIONS + "/" + saveSpecialisation.getId() + "/user", EmbeddedUser.class).getBody();
        Specialisation retrieveSpecialisation = testRestTemplate.getForEntity(SPECIALISATIONS + "/" + saveSpecialisation.getId(), Specialisation.class).getBody();
        //then
        Assertions.assertNotNull(retrieveUser);
        Assertions.assertNotNull(retrieveUser.getEmbedded());
        Assertions.assertEquals(List.of(savedUser), retrieveUser.getEmbedded().getUsers());
        Assertions.assertEquals(saveSpecialisation, retrieveSpecialisation);
    }

    @Test
    void specialisationIsDeleted_receiveSpecialisationInformation_noData() {
        //given
        Specialisation saveSpecialisation = testRestTemplate.postForEntity(SPECIALISATIONS, EntityUtil.specialisation, Specialisation.class).getBody();

        //when
        Assertions.assertNotNull(saveSpecialisation);
        testRestTemplate.delete(SPECIALISATIONS + "/" + saveSpecialisation.getId());
        Specialisation retrievedSpecialisation = testRestTemplate.getForEntity(SPECIALISATIONS + "/" + saveSpecialisation.getId(), Specialisation.class).getBody();

        //then
        Assertions.assertNull(retrievedSpecialisation);
    }

    @Test
    void specialisationIsUpdated_receiveSpecialisationInformation_dataOk() {
        //given
        Specialisation saveSpecialisation = testRestTemplate.postForEntity(SPECIALISATIONS, EntityUtil.specialisation, Specialisation.class).getBody();

        //when
        Assertions.assertNotNull(saveSpecialisation);
        saveSpecialisation.setSpecialisationName(SpecialisationName.CHIRURG);
        testRestTemplate.put(SPECIALISATIONS + "/" + saveSpecialisation.getId(), saveSpecialisation);
        Specialisation retrievedSpecialisation = testRestTemplate.getForEntity(SPECIALISATIONS + "/" + saveSpecialisation.getId(), Specialisation.class).getBody();

        //then
        Assertions.assertEquals(saveSpecialisation, retrievedSpecialisation);
    }
}
