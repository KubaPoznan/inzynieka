package pl.wsb.inzynierka.backend.utils;

import lombok.experimental.UtilityClass;
import pl.wsb.inzynierka.backend.exam.Exam;
import pl.wsb.inzynierka.backend.facility.Facility;
import pl.wsb.inzynierka.backend.message.Message;
import pl.wsb.inzynierka.backend.office.Office;
import pl.wsb.inzynierka.backend.role.Role;
import pl.wsb.inzynierka.backend.role.RoleType;
import pl.wsb.inzynierka.backend.specialisation.Specialisation;
import pl.wsb.inzynierka.backend.specialisation.SpecialisationName;
import pl.wsb.inzynierka.backend.user.User;
import pl.wsb.inzynierka.backend.visit.Status;
import pl.wsb.inzynierka.backend.visit.Visit;


@UtilityClass
public class EntityUtil {
    public static final Exam exam = Exam.builder()
            .name("Badanie prostaty")
            .date(TimeUtil.now)
            .build();

    public static final Facility facility = Facility.builder()
            .city("Poznań")
            .street("Polska")
            .buildingNumber(30)
            .localNumber(10)
            .build();

    public static final User user = new User()
            .setFirstName("Jan")
            .setLastName("Kowalski")
            .setPeselNumber("123")
            .setEmail("j@kowalski.pl")
            .setPassword("Jkowalski");

    public static final Visit visit = Visit.builder()
            .date(TimeUtil.now)
            .status(Status.ACCEPTED)
            .build();

    public static final Role role = Role.builder()
            .roleType(RoleType.PATIENT)
            .build();

    public static final Office office = Office.builder()
            .number(1)
            .build();

    public static final Specialisation specialisation = Specialisation.builder()
            .specialisationName(SpecialisationName.GINEKOLOG)
            .build();

    public static final Message message = Message.builder()
            .title("Zapytanie")
            .content("Czy mogę płacić kartą?")
            .date(TimeUtil.now)
            .build();
}
