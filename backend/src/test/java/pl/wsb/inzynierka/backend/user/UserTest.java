package pl.wsb.inzynierka.backend.user;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import pl.wsb.inzynierka.backend.utils.EntityUtil;
import static org.assertj.core.api.Assertions.assertThat;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;
import static pl.wsb.inzynierka.backend.utils.LinkUtil.USERLOGIN;
import static pl.wsb.inzynierka.backend.utils.LinkUtil.USERS;

@SpringBootTest(webEnvironment = DEFINED_PORT)
class UserTest {

    TestRestTemplate testRestTemplate = new TestRestTemplate();

    @Test
    void userIsAdded_receiveUserInformation_dataIsOk() {
        //given
        User savedUser = testRestTemplate.postForEntity(USERS, EntityUtil.user, User.class).getBody();

        //when
        Assertions.assertNotNull(savedUser);
        User retrievedUser = testRestTemplate.getForEntity(USERS + "/" + savedUser.getId(), User.class).getBody();

        //then
        Assertions.assertEquals(savedUser, retrievedUser);
    }

    @Test
    void userIsDeleted_receiveUserInformation_noData() {
        //given
        User savedUser = testRestTemplate.postForEntity(USERS, EntityUtil.user, User.class).getBody();

        //when
        Assertions.assertNotNull(savedUser);
        testRestTemplate.delete(USERS + "/" + savedUser.getId());
        User retrievedUser = testRestTemplate.getForEntity(USERS + "/" + savedUser.getId(), User.class).getBody();

        //then
        Assertions.assertNull(retrievedUser);
    }

    @Test
    void userIsUpdated_receiveUserInformation_dataOk() {
        //given
        User savedUser = testRestTemplate.postForEntity(USERS, EntityUtil.user, User.class).getBody();

        //when
        Assertions.assertNotNull(savedUser);
        savedUser.setEmail("kowalski@j.pl");
        testRestTemplate.put(USERS + "/" + savedUser.getId(), savedUser);
        User retrievedUser = testRestTemplate.getForEntity(USERS + "/" + savedUser.getId(), User.class).getBody();

        //then
        Assertions.assertEquals(savedUser, retrievedUser);
    }

    @Test
    void userIsGet_userExistInDateBase_dataOk() {
        //given
        User savedUser = testRestTemplate.postForEntity(USERS, EntityUtil.user, User.class).getBody();

        //when
        Assertions.assertNotNull(savedUser);
        ResponseEntity<User> responseEntity =
                testRestTemplate.getForEntity(USERLOGIN + "?email={0}&password={1}",
                        User.class,
                        EntityUtil.user.getEmail(),
                        EntityUtil.user.getPassword());


        //then
        assertThat(savedUser).usingRecursiveComparison()
                .ignoringFields("visitDoctor","visitPatient","examPatient","examDoctor","role","specialisation","messageSender","messageReceiver")
                .isEqualTo(responseEntity.getBody());
        //Assertions.assertEquals(savedUser, responseEntity.getBody());
        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }
}
