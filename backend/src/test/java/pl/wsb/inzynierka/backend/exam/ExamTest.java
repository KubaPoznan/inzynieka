package pl.wsb.inzynierka.backend.exam;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import pl.wsb.inzynierka.backend.facility.Facility;
import pl.wsb.inzynierka.backend.user.User;
import pl.wsb.inzynierka.backend.utils.EntityUtil;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;
import static pl.wsb.inzynierka.backend.utils.HttpUtil.getHttpEntity;
import static pl.wsb.inzynierka.backend.utils.LinkUtil.*;


@SpringBootTest(webEnvironment = DEFINED_PORT)
class ExamTest {

    TestRestTemplate testRestTemplate = new TestRestTemplate();

    @Test
    void examIsAdded_retrievedExamInformation_dataIsOk() {
        //given

        Exam savedExam = testRestTemplate
                .postForEntity(EXAMS, EntityUtil.exam, Exam.class).getBody();

        Facility savedFacility = testRestTemplate
                .postForEntity(FACILITIES, EntityUtil.facility, Facility.class).getBody();

        User savedUser = testRestTemplate
                .postForEntity(USERS, EntityUtil.user, User.class).getBody();

        //when
        Assertions.assertNotNull(savedExam);
        Assertions.assertNotNull(savedFacility);
        Assertions.assertNotNull(savedUser);

        testRestTemplate.put(EXAMS + "/" + savedExam.getId() + "/facility", getHttpEntity(FACILITIES + "/" + savedFacility.getId()));
        testRestTemplate.put(EXAMS + "/" + savedExam.getId() + "/doctor", getHttpEntity(USERS + "/" + savedUser.getId()));
        testRestTemplate.put(EXAMS + "/" + savedExam.getId() + "/patient", getHttpEntity(USERS + "/" + savedUser.getId()));

        Exam retrievedExam = testRestTemplate
                .getForEntity(EXAMS + "/" + savedExam.getId(), Exam.class).getBody();

        Facility retrievedFacility = testRestTemplate
                .getForEntity(EXAMS + "/" + savedExam.getId() + "/facility", Facility.class).getBody();

        User retrievedDoctor = testRestTemplate
                .getForEntity(EXAMS + "/" + savedExam.getId() + "/doctor", User.class).getBody();

        User retrievedPatient = testRestTemplate
                .getForEntity(EXAMS + "/" + savedExam.getId() + "/patient", User.class).getBody();

        //then
        Assertions.assertNotNull(retrievedExam);
        assertThat(savedExam.getDate()).isEqualToIgnoringNanos(retrievedExam.getDate());
        assertThat(savedExam).usingRecursiveComparison().ignoringFields("date").isEqualTo(retrievedExam);
        assertThat(savedFacility).isEqualTo(retrievedFacility);
        assertThat(savedUser).isEqualTo(retrievedDoctor).isEqualTo(retrievedPatient);


    }

    @Test
    void examIsDeleted_retrievedExamInformation_noData() {
        //given

        Exam savedExam = testRestTemplate
                .postForEntity(EXAMS, EntityUtil.exam, Exam.class).getBody();

        //when
        Assertions.assertNotNull(savedExam);
        testRestTemplate.delete(EXAMS + "/" + savedExam.getId());
        Exam retrievedExam = testRestTemplate
                .getForEntity(EXAMS + "/" + savedExam.getId(), Exam.class).getBody();

        //then
        Assertions.assertNull(retrievedExam);

    }

    @Test
    void examIsUpdated_retrievedExamInformation_dataOk() {
        //given
        Exam savedExam = testRestTemplate
                .postForEntity(EXAMS, EntityUtil.exam, Exam.class).getBody();

        //when
        Assertions.assertNotNull(savedExam);
        savedExam.setName("Gastroskopia");
        testRestTemplate.put(EXAMS + "/" + savedExam.getId(), savedExam);
        Exam retrievedExam = testRestTemplate
                .getForEntity(EXAMS + "/" + savedExam.getId(), Exam.class).getBody();

        //then
        Assertions.assertNotNull(retrievedExam);
        assertThat(savedExam.getDate()).isEqualToIgnoringNanos(retrievedExam.getDate());
        assertThat(savedExam).usingRecursiveComparison().ignoringFields("date").isEqualTo(retrievedExam);
    }
}
