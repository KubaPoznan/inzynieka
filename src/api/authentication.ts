import {UserLoginData, UserRegistrationData} from '../resources/userData.resource'

const baseUrl = process.env.REACT_APP_API_URL;
const registrationUrl = `${baseUrl}/register`
const loginUrl = `${baseUrl}/signin`

export const registerUser = (data: UserRegistrationData): Promise<Response> => {
  return fetch(registrationUrl, { method: 'POST', body: JSON.stringify(data), headers: {'Content-Type': 'application/json'}});
}

export const loginUser = (data: UserLoginData): Promise<Response> => {
  return fetch(loginUrl, {method: 'POST', body: JSON.stringify(data), headers: {'Content-Type': 'application/json'}});
}