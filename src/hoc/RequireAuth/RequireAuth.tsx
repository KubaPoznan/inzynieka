import { Navigate } from 'react-router-dom';
import { useAppSelector } from '../../hooks';
import { selectLoggedInUser } from '../../store/userStore/user.selectors';

export const RequireAuth = ({
	children,
}: {
	children: JSX.Element;
}): JSX.Element => {
	const loggedInUser = useAppSelector(selectLoggedInUser);

	if (loggedInUser?.email) {
		return children;
	}

	return <Navigate to='/logowanie' />;
};
