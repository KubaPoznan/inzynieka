import { Gallery, Cards, HeroSection } from '../components';
import { Fragment } from 'react';
export const Home = () => (
	<Fragment>
		<HeroSection />
		<Cards />
		<Gallery />
	</Fragment>
);
