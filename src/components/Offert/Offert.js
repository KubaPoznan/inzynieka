import React from "react";
import Collapsible from "./Collapsible";
import "./Offert.css";

export const Offert = () => {
  return (
    <div>
     
        <div className="offert-container">
          <div className="offerts">
            Oferty
            <hr />
          </div>
          <br />
          <br />
          <div className="offert-info">
            W przychodni oprócz wizyt na NFZ możliwe jest również umówienie się
            na wizytę prywatną, poniżej przedstawiamy cennik usług, które
            oferują nasi specjaliści.
          </div>
          <div className="main-row">
            <Collapsible doc="Laryngologia - lek. Jan Kowalski" label="+">
              <br />
              <b>Konsultacje</b>
              <hr />
              Konsultacje&emsp; &ensp; 100zł
              <br /> <br />
              <b>Badania</b>
              <hr />
              Badanie 1 &emsp; &emsp; 100zł <br />
              Badanie 2 &emsp; &emsp; 100zł <br />
              Badanie 3 &emsp; &emsp; 150zł <br />
              Badanie 4 &emsp; &emsp; 200zł <br />
              <br />
            </Collapsible>
          </div>
          <div className="main-row">
            <Collapsible doc="Kardiologia - lek. Jan Kowalski" label="+">
              <br />
              <b>Konsultacje</b>
              <hr />
              Konsultacje &emsp; &emsp; 100zł
              <br /> <br />
              <b>Badania</b>
              <hr />
              Badanie 1 &emsp; &emsp; 100zł <br />
              Badanie 2 &emsp; &emsp; 100zł <br />
              Badanie 3 &emsp; &emsp; 150zł <br />
              Badanie 4 &emsp; &emsp; 200zł <br />
              <br />
            </Collapsible>
          </div>
          <div className="main-row">
            <Collapsible doc="Chirurgia - lek. Jan Kowalski" label="+">
              <br />
              <b>Konsultacje</b>
              <hr />
              Konsultacje &emsp; &emsp; 100zł
              <br /> <br />
              <b>Badania</b>
              <hr />
              Badanie 1 &emsp; &emsp; 100zł <br />
              Badanie 2 &emsp; &emsp; 100zł <br />
              Badanie 3 &emsp; &emsp; 150zł <br />
              Badanie 4 &emsp; &emsp; 200zł <br />
              <br />
            </Collapsible>
          </div>
          <div className="main-row">
            <Collapsible doc="Alergologia - lek. Jan Kowalski" label="+">
              <br />
              <b>Konsultacje</b>
              <hr />
              Konsultacje &emsp; &emsp; 100zł
              <br /> <br />
              <b>Badania</b>
              <hr />
              Badanie 1 &emsp; &emsp; 100zł <br />
              Badanie 2 &emsp; &emsp; 100zł <br />
              Badanie 3 &emsp; &emsp; 150zł <br />
              Badanie 4 &emsp; &emsp; 200zł <br />
              <br />
            </Collapsible>
          </div>
        </div>
      </div>
    
  );
};
