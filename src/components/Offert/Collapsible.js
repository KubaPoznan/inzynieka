import React, { useState } from "react";

function Collapsible(props) {
  const [isOpen, setIsOpen] = useState(false);
  const znak = "...";
  return (
    <div className="collapsible">
      <div className="doctor-name">{props.doc}</div>
      <div className="doctor-button">
        <button
          className="toggle"
          onClick={() => setIsOpen(!isOpen)}
        >
          {znak}
        </button>
      </div>
      {isOpen && <div className="content"> {props.children}</div>}
    </div>
  );
}
export default Collapsible;
