import "./Specialization.css";
import chirurg from "../../assets/photos/chirurg.png";
import kardiolog from "../../assets/photos/kardiolog.png";
import laryngolog from "../../assets/photos/laryngolog.png";
import neurolog from "../../assets/photos/neurolog.png";
import onkolog from "../../assets/photos/onkolog.png";
import ortopeda from "../../assets/photos/ortopeda.png";
import pediatra from "../../assets/photos/pediatra.png";
import psycholog from "../../assets/photos/psycholog.png";
import pulmonolog from "../../assets/photos/pulmonolog.png";

export const  Specialization = () => {
  return (
    <div className="specialization-container">
      <div className="specialization-info">
        <h2>Specjalizacje</h2>
        W JPWK Health oferujemy konsultacje ze specjalistą w jednej z 7
        <br /> dziedzin. Nasi specjaliści zapewniają specjalistyczną i
        <br />
        profesjonalną opiekę.
        <br />
        <br />
      </div>
        <div className="grid-container">
        <div className="grid">
          <article>
            <img src={chirurg} alt="" />
            <div className="text">
              <h3>Chirurg ogólny </h3>
            </div>
          </article>
          <article>
            <img src={kardiolog} alt="" />
            <div className="text">
              <h3>Kardiolog </h3>
            </div>
          </article>
          <article>
            <img src={laryngolog} alt="" />
            <div className="text">
              <h3>Laryngolog </h3>
            </div>
          </article>
          <article>
            <img src={neurolog} alt="" />
            <div className="text">
              <h3>Neurolog </h3>
            </div>
          </article>
          <article>
            <img src={onkolog} alt="" />
            <div className="text">
              <h3>Onkolog </h3>
            </div>
          </article>
          <article>
            <img src={ortopeda} alt="" />
            <div className="text">
              <h3>Ortopeda </h3>
            </div>
          </article>
          <article>
            <img src={pediatra} alt="" />
            <div className="text">
              <h3>Pediatra </h3>
            </div>
          </article>
          <article>
            <img src={psycholog} alt="" />
            <div className="text">
              <h3>Psycholog </h3>
            </div>
          </article>
          <article>
            <img src={pulmonolog} alt="" />
            <div className="text">
              <h3>Pulmonolog </h3>
            </div>
          </article>
          <br/>
          <br/>
          <br/>
          </div>
        </div>
      </div>
    
  );
}
