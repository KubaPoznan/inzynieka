import './ContactCard.css';
import buildPhoto from '../../assets/photos/pexels-pixabay-269077.jpg' 
const dataTable = [
    
]
export const ContactCard = () => {
    return(
        <div className="contactCard">
            <div className="firstRect">
                <div className="secoudRect">
                    <h1 className="header">
                        Kontakt
                    </h1>
                    <h4 className="Name">
                        JPWK Health
                    </h4>
                    <p className="addressData">Głogowska 3, 60-104 Poznań</p>
                    <p className="addressData">123456789</p>
                    <p className="addressData">jpwkhealt@jpwkhealt.com</p>
                    <img src={buildPhoto} alt="photo" className="photo" />

                </div>
            </div>
        </div>
    );
};