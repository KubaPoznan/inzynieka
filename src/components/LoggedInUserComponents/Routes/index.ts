export { patientRoutes } from './PatientRoutes';
export { doctorRoutes } from './DoctorRoutes';
export { recorderRoutes } from './RecorderRoutes';