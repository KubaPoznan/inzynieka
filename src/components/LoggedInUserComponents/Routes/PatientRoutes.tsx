import { Route } from 'react-router-dom';

// KOMENTARZ: Postaraj sie znalezc lepsze miejsce dla tych komponentow/stałych
export const patientRoutes = (
	<>
		<Route
			path='/czat-z-lekarzem'
			element={<div>Replace me with Real Component, please!</div>}
		/>
		<Route
			path='/moje-wizyty'
			element={<div>Replace me with Real Component, please!</div>}
		/>
		<Route
			path='/zapytaj-lekarza'
			element={<div>Replace me with Real Component, please!</div>}
		/>
		<Route
			path='/dokumentacja'
			element={<div>Replace me with Real Component, please!</div>}
		/>
		<Route
			path='wyniki-badan'
			element={<div>Replace me with Real Component, please!</div>}
		/>
		<Route
			path='/leki-i-e-recepty'
			element={<div>Replace me with Real Component, please!</div>}
		/>
		<Route
			path='/skierowania'
			element={<div>Replace me with Real Component, please!</div>}
		/>
	</>
);
