import { Link } from 'react-router-dom';

export const PatientNavigation = () => {
	return (
		<>
			<Link to={'czat-z-lekarzem'}> Czat z lekarzem </Link>
			<Link to={'moje-wizyty'}> Moje wizyty </Link>
			<Link to={'zapytaj-lekarza'}> Zapytaj lekarza </Link>
			<Link to={'dokumentacja'}> Dokumentacja </Link>
			<Link to={'wyniki-badan'}> Wyniki Badań </Link>
			<Link to={'leki-i-e-recepty'}> Leki i e-recepty </Link>
			<Link to={'skierowania'}> Skierownia </Link>
		</>
	);
};
