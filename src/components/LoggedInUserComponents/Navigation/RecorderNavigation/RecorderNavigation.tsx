import { Link } from 'react-router-dom';

export const RecorderNavigation = () => {
  return (
    <>
			<Link to={'pacjenci'}> Pacjenci </Link>
			<Link to={'harmonogram'}> Harmonogram </Link>
			<Link to={'wiadomosci'}> Wiadomości </Link>
    </>
  )
}