import { Link } from 'react-router-dom';
export const DoctorNavigation = () => {
	return (
		<>
			<Link to={'pacjenci'}> Pacjenci </Link>
			<Link to={'harmonogram'}> Harmonogram </Link>
			<Link to={'wiadomosci'}> Wiadomości </Link>
			<Link to={'baza-lekow'}> Baza Leków </Link>
		</>
	);
};
