import './UpcomingVisits.css';
import { UpcomingVisitsResource } from '../../../resources/upcomingVisits.resource';
import { DoctorVisits, PatientVisits, RecorderVisits } from '..';
import { UserRole } from '../../../resources/userRoles.enum';

export const UpcomingVisits = ({
	data,
	role,
}: {
	data: UpcomingVisitsResource[];
	role: string[];
}) => {
	let component: JSX.Element = <></>;

	switch (role[0]) {
		case UserRole.DOCTOR:
			component = <DoctorVisits data={data} />;
			break;
		case UserRole.RECORDER:
			component = <RecorderVisits data={data} />;
			break;
		case UserRole.PATIENT:
			component = <PatientVisits data={data} />;
			break;
		default:
			break;
	}

	return component;
};
