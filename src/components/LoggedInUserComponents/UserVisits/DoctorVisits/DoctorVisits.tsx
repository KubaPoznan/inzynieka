import { UpcomingVisitsResource } from '../../../../resources/upcomingVisits.resource';
import '../../tables.css';

export const DoctorVisits = ({ data }: { data: UpcomingVisitsResource[] }) => {
	function renderTableData(data: UpcomingVisitsResource[]): JSX.Element[] {
		return data.map((visit) => {
			const { id, patient, status, location, timeStamp } = visit;

			return (
				<tr key={id}>
					<td>{patient}</td>
					<td>{status ? 'Potwierdzona' : 'Niepotwierdzona'}</td>
					<td>{location}</td>
					<td>{timeStamp}</td>
					<td>Rozpocznij</td>
					<td>Odmow</td>
				</tr>
			);
		});
	}

	return (
		<div className='visits__table recorder-visits__table'>
			<table className='table'>
				<tbody>
					<tr>
						<th>Pacjent</th>
						<th>Status</th>
						<th>Lokalizacja</th>
						<th>Data i czas</th>
						<th></th>
						<th></th>
					</tr>
					{renderTableData(data)}
				</tbody>
			</table>
		</div>
	);
};
