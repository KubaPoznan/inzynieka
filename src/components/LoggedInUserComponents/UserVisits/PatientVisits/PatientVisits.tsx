import { UpcomingVisitsResource } from '../../../../resources/upcomingVisits.resource';
import '../../tables.css';

export const PatientVisits = ({ data }: { data: UpcomingVisitsResource[] }) => {
	function renderTableData(data: UpcomingVisitsResource[]): JSX.Element[] {
		return data.map((visit) => {
			const { id, doctor, specialization, location, timeStamp, status } = visit;

			return (
				<tr key={id}>
					<td>{doctor}</td>
					<td>{specialization}</td>
					<td>{location}</td>
					<td>{timeStamp}</td>
					<td>{status ? 'Potwierdzona' : 'Niepotwierdzona'}</td>
					<td>Potwierdz</td>
					<td>Odmow</td>
				</tr>
			);
		});
	}

	return (
		<div className='visits__table recorder-visits__table'>
			<table className='table'>
				<tbody>
					<tr>
						<th>Lekarz</th>
						<th>Specjalizacja</th>
						<th>Lokalizacja</th>
						<th>Data i czas</th>
						<th>Status</th>
						<th></th>
						<th></th>
					</tr>
					{renderTableData(data)}
				</tbody>
			</table>
		</div>
	);
};
