import { UpcomingVisitsResource } from '../../../../resources/upcomingVisits.resource';
import '../../tables.css';

export const RecorderVisits = ({
	data,
}: {
	data: UpcomingVisitsResource[];
}) => {
	function renderTableData(data: UpcomingVisitsResource[]): JSX.Element[] {
		return data.map((visit) => {
			const { id, patient, doctor, location, timeStamp, status } = visit;

			return (
				<tr key={id}>
					<td>{patient}</td>
					<td>{doctor}</td>
					<td>{location}</td>
					<td>{timeStamp}</td>
					<td>{status ? 'Potwierdzona' : 'Niepotwierdzona'}</td>
					<td>Button placeholder</td>
				</tr>
			);
		});
	}

	return (
		<div className='visits__table recorder-visits__table'>
			<table className='table'>
				<tbody>
					<tr>
						<th>Pacjent</th>
						<th>Lekarz</th>
						<th>Lokalizacja</th>
						<th>Data i czas</th>
						<th>Status</th>
						<th></th>
					</tr>
					{renderTableData(data)}
				</tbody>
			</table>
		</div>
	);
};
