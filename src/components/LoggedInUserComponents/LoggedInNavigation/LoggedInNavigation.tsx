import { Link } from 'react-router-dom';
import { UserRole } from '../../../resources/userRoles.enum';
import {
	DoctorNavigation,
	PatientNavigation,
	RecorderNavigation,
} from '../Navigation';
import './LoggedInNavigation.css';

export const LoggedInNavigation = ({ userRoles }: { userRoles: string[] }) => {
	let navigation: JSX.Element = <></>;

	switch (userRoles[0]) {
		case UserRole.DOCTOR:
			navigation = <DoctorNavigation />;
			break;
		case UserRole.RECORDER:
			navigation = <RecorderNavigation />;
			break;
		case UserRole.PATIENT:
			navigation = <PatientNavigation />;
			break;
		default:
			break;
	}

	//KOMENTARZ: dodac link active

	return (
		<div className='logged-in-navigation__wrapper'>
			<div className='logged-in-navigation__logo'></div>
			<div className='logged-in-navigation__links'>
				<Link to={''}> Stronga główna </Link>
				{navigation}
				<Link to={'ustawienia'}> Ustawienia </Link>
			</div>
		</div>
	);
};
