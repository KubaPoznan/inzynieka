import { PatientResultsResource } from '../../../../resources/patientResults.resource';
import '../../tables.css';

export const PatientResults = ({
	results,
}: {
	results: PatientResultsResource[];
}) => {
	function renderTableData(results: PatientResultsResource[]): JSX.Element[] {
		return results.map((result) => {
			const { id, examination, timeStamp, location, doctor } = result;

			return (
				<tr key={id}>
					<td>{examination}</td>
					<td>{timeStamp}</td>
					<td>{location}</td>
					<td>{doctor}</td>
					<td>Podgląd</td>
				</tr>
			);
		});
	}

	return (
		<div className='patient-results__table'>
			<table className='table'>
				<tbody>
					<tr>
						<th>Badanie</th>
						<th>Data</th>
						<th>Lokalizacja</th>
						<th>Zlecił</th>
						<th></th>
					</tr>
					{renderTableData(results)}
				</tbody>
			</table>
		</div>
	);
};
