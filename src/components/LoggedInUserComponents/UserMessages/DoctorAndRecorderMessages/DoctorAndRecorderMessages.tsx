import { DoctorAndRecorderMesssagesResource } from '../../../../resources/doctorAndRecorderMessages.resource';
import '../../tables.css';

export const DoctorAndRecorderMessages = ({
	messages,
}: {
	messages: DoctorAndRecorderMesssagesResource[];
}) => {
	function renderTableData(
		messages: DoctorAndRecorderMesssagesResource[]
	): JSX.Element[] {
		return messages.map((message) => {
			const { id, sender, topic, messageBody } = message;

			return (
				<tr key={id}>
					<td>{sender}</td>
					<td>{topic}</td>
					<td>{messageBody}</td>
					<td>Podgląd</td>
				</tr>
			);
		});
	}

	return (
		<div className='patient-results__table'>
			<table className='table'>
				<tbody>
					<tr>
						<th>Nadawca</th>
						<th>Temat</th>
						<th>Treść</th>
						<th></th>
					</tr>
					{renderTableData(messages)}
				</tbody>
			</table>
		</div>
	);
};
