export interface DoctorAndRecorderMesssagesResource {
  id: number;
  sender: string;
  topic: string;
  messageBody: string;
}