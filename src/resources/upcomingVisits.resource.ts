export interface UpcomingVisitsResource {
  id: number;
  patient?: string;
  doctor?: string;
  specialization?: string;
  location: string;
  timeStamp: number;
  status: boolean;
}