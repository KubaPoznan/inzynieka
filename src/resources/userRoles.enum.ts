export enum UserRole {
  RECORDER = "RECORDER",
  PATIENT = "PATIENT",
  DOCTOR = "DOCTOR"
}