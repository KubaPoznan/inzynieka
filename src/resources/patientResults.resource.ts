export interface PatientResultsResource {
  id: number;
  examination: string;
  timeStamp: number;
  location: string;
  doctor: string
}