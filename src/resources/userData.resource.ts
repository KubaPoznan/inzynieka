export interface UserRegistrationData {
  login: string;
  firstName: string;
  lastName: string;
  peselNumber: number;
  email: string;
  password: string;
}

export interface UserLoginData {
  email: string;
  password: string;
}

export interface LoggedInUserData {
  accessToken?: string;
  user?: UserData
}

export interface UserData {
  id: number
  email: string;
  firstName: string;
  lastName: string;
  login: string;
  peselNumber: number;
  roles: string[];
}