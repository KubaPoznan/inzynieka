import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { Home, LoginPage, RegisternPage, LoggedInUserPage,Contact, OffertsPage, SpecializationPage, PatientPage } from './pages';
import { Navbar, Footer } from './components';
import { RequireAuth } from './hoc/RequireAuth/RequireAuth';

export const App = () => (
	<div className='App'>
		<Router>
			<Navbar />
			<Routes>
				<Route path='/' element={<Home />} />
				<Route path='/logowanie' element={<LoginPage />} />
				<Route path='/rejestracja' element={<RegisternPage />} />
				<Route path='/kontakt' element={<Contact />} />
				<Route path='/oferta' element={<OffertsPage />} />
				<Route path='/wizyty' element={<OffertsPage />} />
				<Route path='/specjalizacje' element={<PatientPage  />} />
				<Route path='/dd' element={<PatientPage  />} />
				<Route
					path='/zalogowani/*'
					element={
						<RequireAuth>
							<LoggedInUserPage />
						</RequireAuth>
					}
				/>
			</Routes>
			<Footer />
		</Router>
	</div>
);
