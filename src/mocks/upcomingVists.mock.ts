import { UpcomingVisitsResource } from "../resources/upcomingVisits.resource";

export const upcomingVisitsMock: UpcomingVisitsResource[] = [
  {
    id: 1,
    location: 'Poznań, Dmowskiego 109a, gabinet 16',
    status: true,
    timeStamp: Date.now(),
    doctor: 'Jacek Krause',
    specialization: 'Internista',
    patient: 'Jacek Krauze' 
  },
  {
    id: 2,
    location: 'Poznań, Dmowskiego 109a, gabinet 16',
    status: false,
    timeStamp: Date.now(),
    doctor: 'Grzegorz Nowak',
    specialization: 'Internista',
    patient: 'Maciej Nowak' 
  },
  {
    id: 3,
    location: 'Poznań, Dmowskiego 109a, gabinet 16',
    status: true,
    timeStamp: Date.now(),
    doctor: 'Jacek Krause',
    specialization: 'Internista',
    patient: 'Jacek Krauze' 
  },
  {
    id: 4,
    location: 'Poznań, Dmowskiego 109a, gabinet 16',
    status: false,
    timeStamp: Date.now(),
    doctor: 'Grzegorz Nowak',
    specialization: 'Internista',
    patient: 'Maciej Nowak' 
  },
  {
    id: 5,
    location: 'Poznań, Dmowskiego 109a, gabinet 16',
    status: true,
    timeStamp: Date.now(),
    doctor: 'Jacek Krause',
    specialization: 'Internista',
    patient: 'Jacek Krauze' 
  },
  {
    id: 6,
    location: 'Poznań, Dmowskiego 109a, gabinet 16',
    status: false,
    timeStamp: Date.now(),
    doctor: 'Grzegorz Nowak',
    specialization: 'Internista',
    patient: 'Maciej Nowak' 
  }
]