import { DoctorAndRecorderMesssagesResource } from "../resources/doctorAndRecorderMessages.resource";

export const doctorAndRecorderMessagesMock: DoctorAndRecorderMesssagesResource[] = [
  {
    id: 1,
    sender: 'Jacek Krause',
    messageBody: 'Pacjent umarł',
    topic: 'Co sie dzieje wsród pacjentów'
  },
  {
    id: 2,
    sender: 'Jacek Krause',
    messageBody: 'Pacjent umarł',
    topic: 'Co sie dzieje wsród pacjentów'
  },
  {
    id: 3,
    sender: 'Jacek Krause',
    messageBody: 'Pacjent umarł',
    topic: 'Co sie dzieje wsród pacjentów'
  },
  {
    id: 4,
    sender: 'Jacek Krause',
    messageBody: 'Pacjent umarł',
    topic: 'Co sie dzieje wsród pacjentów'
  }
]