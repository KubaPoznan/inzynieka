import { PatientResultsResource } from "../resources/patientResults.resource";

export const patientResultsMock: PatientResultsResource[] = [
  {
    id: 1,
    doctor: 'Jacek Krause',
    examination: 'Wydobycie ciała obcego',
    location: 'Poznan',
    timeStamp: Date.now()
  },
  {
    id: 2,
    doctor: 'Jacek Krause',
    examination: 'Wydobycie ciała obcego',
    location: 'Poznan',
    timeStamp: Date.now()
  },
  {
    id: 3,
    doctor: 'Jacek Krause',
    examination: 'Wydobycie ciała obcego',
    location: 'Poznan',
    timeStamp: Date.now()
  },
  {
    id: 4,
    doctor: 'Jacek Krause',
    examination: 'Wydobycie ciała obcego',
    location: 'Poznan',
    timeStamp: Date.now()
  }
]