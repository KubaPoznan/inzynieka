import React, { FormEvent, useState } from 'react';
import { useAppDispatch } from '../../hooks';
import { UserRegistrationData } from '../../resources/userData.resource';
import { registerUser } from '../../store/userStore/user.actions';
import './Register.css';

export const Register = () => {
	const initialState: UserRegistrationData = {
		login: '',
		firstName: '',
		lastName: '',
		peselNumber: 0,
		email: '',
		password: '',
	} as UserRegistrationData;

	const [formData, setFormData] = useState(initialState);
	const dispatch = useAppDispatch();

	function handleSubmit(event: FormEvent<HTMLFormElement>) {
		event.preventDefault();
		dispatch(registerUser(formData));
	}

	function handleChange(event: React.ChangeEvent<HTMLInputElement>) {
		setFormData({ ...formData, [event.target.name]: event.target.value });
	}

	return (
		<div className='login-container'>
			<div className='login-bg'>
				<h1 className='login-h1'>Rejestracja</h1>
				<div className='login-panel'>
					<form onSubmit={handleSubmit}>
						<div className='login-input'>
							<input
								type='text'
								placeholder='Login'
								name='login'
								value={formData.login}
								onChange={handleChange}
							></input>
						</div>
						<div className='login-input'>
							<input
								type='text'
								placeholder='Imię'
								name='firstName'
								value={formData.firstName}
								onChange={handleChange}
							></input>
						</div>
						<div className='login-input'>
							<input
								type='text'
								placeholder='Nazwisko'
								name='lastName'
								value={formData.lastName}
								onChange={handleChange}
							></input>
						</div>
						<div className='login-input'>
							<input
								type='number'
								placeholder='PESEL'
								maxLength={11}
								name='peselNumber'
								value={formData.peselNumber || ''}
								onChange={handleChange}
							></input>
						</div>
						<div className='login-input'>
							<input
								type='text'
								placeholder='E-mail'
								name='email'
								value={formData.email}
								onChange={handleChange}
							></input>
						</div>
						<div className='login-input'>
							<input
								type='password'
								placeholder='Hasło'
								name='password'
								value={formData.password}
								onChange={handleChange}
							></input>
						</div>
						<div className='login-btn'>
							<input type='submit' value='Zarejestruj' />
						</div>
					</form>
				</div>
			</div>
		</div>
	);
};
