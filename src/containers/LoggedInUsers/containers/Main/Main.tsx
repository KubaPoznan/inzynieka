import './Main.css';
import { upcomingVisitsMock } from '../../../../mocks/upcomingVists.mock';
import { patientResultsMock } from '../../../../mocks/patientResults.mock';
import { doctorAndRecorderMessagesMock } from '../../../../mocks/doctorAndRecorderMessages.mock';
import {
	DoctorAndRecorderMessages,
	PatientResults,
	UpcomingVisits,
} from '../../../../components';
import { UserRole } from '../../../../resources/userRoles.enum';

export const Main = ({ userRoles }: { userRoles: string[] }): JSX.Element => {
	return (
		<div className='main'>
			<div className='main__upcoming-visits'>
				<p className='main__upcoming-visits__title'>Nadchodzące wizyty</p>
				<div className='main__upcoming-visits__container'>
					<UpcomingVisits
						data={upcomingVisitsMock}
						role={userRoles}
					></UpcomingVisits>
				</div>
				<div className='main__messages__container'>
					{userRoles[0] === UserRole.PATIENT ? (
						<>
							<p className='main__messages__title'>Wyniki</p>
							<PatientResults results={patientResultsMock} />
						</>
					) : (
						<>
							<p className='main__messages__title'>Nowe Wiadomości</p>
							<DoctorAndRecorderMessages
								messages={doctorAndRecorderMessagesMock}
							/>
						</>
					)}
				</div>
			</div>
		</div>
	);
};
