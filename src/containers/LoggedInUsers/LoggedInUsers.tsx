import { Outlet, Route, Routes } from 'react-router-dom';
import {
	doctorRoutes,
	LoggedInNavigation,
	patientRoutes,
	recorderRoutes,
} from '../../components';
import { useAppSelector } from '../../hooks';
import { UserRole } from '../../resources/userRoles.enum';
import { selectLoggedInUser } from '../../store/userStore/user.selectors';
import { Main } from './containers/Main/Main';
import './LoggedInUsers.css';

export const LoggedInUsersPage = () => {
	const userRoles = useAppSelector(selectLoggedInUser)?.roles ?? [];

	let routes: JSX.Element = <></>;

	switch (userRoles[0]) {
		case UserRole.PATIENT:
			routes = patientRoutes;
			break;
		case UserRole.DOCTOR:
			routes = doctorRoutes;
			break;
		case UserRole.RECORDER:
			routes = recorderRoutes;
			break;
		default:
			break;
	}

	return (
		<div className='logged-in-users'>
			<div className='logged-in-users__navigation'>
				<LoggedInNavigation userRoles={userRoles} />
			</div>
			<div className='logged-in-users__outlet'>
				<Outlet />
				<Routes>
					<Route path='/' element={<Main userRoles={userRoles}></Main>} />
					{routes}
				</Routes>
			</div>
		</div>
	);
};
