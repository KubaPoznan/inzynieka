import { FormEvent, useState } from 'react';
import { Navigate } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from '../../hooks';
import { UserLoginData } from '../../resources/userData.resource';
import { loginUser } from '../../store/userStore/user.actions';
import { selectLoggedInUser } from '../../store/userStore/user.selectors';
import './Login.css';

export const Login = () => {
	const initialState: UserLoginData = { email: '', password: '' };
	const [formData, setFormData] = useState(initialState);
	const dispatch = useAppDispatch();
	const loggedInUser = useAppSelector(selectLoggedInUser);

	function handleChange(event: React.ChangeEvent<HTMLInputElement>) {
		setFormData({ ...formData, [event.target.name]: event.target.value });
	}

	function handleSubmit(event: FormEvent<HTMLFormElement>) {
		event.preventDefault();
		dispatch(loginUser(formData));

		// KOMENTARZ: Zapis uzytkownika/tokena do local/session storage/cookies, tak, zeby nie trzeba bylo za kazdym razem sie logowac
	}

	return (
		<div className='login-container'>
			{loggedInUser?.email && <Navigate to={'/zalogowani'} />}
			<div className='login-bg'>
				<h1 className='login-h1'>Logowanie</h1>
				<div className='login-panel'>
					<form onSubmit={handleSubmit}>
						<div className='login-input'>
							<i className='far fa-user'></i>
							<input
								type='email'
								placeholder='E-mail'
								name='email'
								value={formData.email}
								onChange={handleChange}
							></input>
						</div>
						<div className='login-input'>
							<i className='fas fa-lock'></i>
							<input
								type='password'
								placeholder='Hasło'
								name='password'
								value={formData.password}
								onChange={handleChange}
							></input>
						</div>
						<div className='login-btn'>
							<input type='submit' value='Zaloguj' />
						</div>
					</form>
				</div>
			</div>
		</div>
	);
};
