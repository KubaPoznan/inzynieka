import { applyMiddleware, combineReducers, configureStore} from "@reduxjs/toolkit";
import thunk from "redux-thunk";
import { userReducer } from "./store/userStore/user.reducer";

const createRootReducer = () => combineReducers({
  user: userReducer,
})

export const store = configureStore({
  reducer: createRootReducer(),
  devTools: process.env.NODE_ENV !== 'production',
  enhancers: [applyMiddleware(thunk)],
});


export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;