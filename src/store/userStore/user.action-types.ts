import { createAction } from "@reduxjs/toolkit";
import { LoggedInUserData, UserLoginData, UserRegistrationData } from "../../resources/userData.resource";

export const reqisterUser = createAction<UserRegistrationData>('[User] Register User');
export const registerUserSuccess = createAction('[User] Register User Success'); 
export const registerUserFail = createAction('[User] Register User Fail'); 

export const loginUser = createAction<UserLoginData>('[User] Login User');
export const loginUserSuccess = createAction<LoggedInUserData>('[User] Login User Success');
export const loginUserFail = createAction('[User] Login User Fail');
