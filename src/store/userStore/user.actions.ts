import * as API from "../../api/authentication";
import { LoggedInUserData, UserLoginData, UserRegistrationData } from "../../resources/userData.resource";
import { AppDispatch } from "../../store";
import * as ACTION from "./user.action-types";

// implement error handling
export const loginUser = (userData: UserLoginData) => {
  return async (dispatch: AppDispatch) => {
    dispatch(ACTION.loginUser(userData));
    await API.loginUser(userData)
    .then((response: Response) => {
      if (response.ok) {
        return response.json();
      } else {
        throw new Error('')
      }
    })
    .then((data: LoggedInUserData) => {
      dispatch(ACTION.loginUserSuccess(data))
    })
    .catch(error => {
      dispatch(ACTION.loginUserFail())
    })
  }
};

export const registerUser = (userData: UserRegistrationData) => {
  return async (dispatch: AppDispatch) => {
    dispatch(ACTION.reqisterUser(userData));
    await API.registerUser(userData)
    .then((response: Response) => {
      if (response.ok) {
        return response.json();
      } else {
        throw new Error('')
      }
    })
    .then(() => dispatch(ACTION.registerUserSuccess()))
    .catch(error => dispatch(ACTION.registerUserFail()))
  }
};