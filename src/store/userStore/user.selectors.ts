import { createSelector } from "@reduxjs/toolkit";
import { RootState } from "../../store";

export const selectState = (state: RootState) => state;
export const selectLoggedInUser = createSelector(selectState, state => state.user.loggedInUser);