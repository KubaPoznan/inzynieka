import { createReducer } from '@reduxjs/toolkit';
import { UserData } from "../../resources/userData.resource";
import * as Action from './user.action-types';

interface UserState {
  loggedInUser?: UserData
}

const initialState: UserState = {
  loggedInUser: undefined
}

export const userReducer = createReducer(initialState, (builder) => {
  builder.addCase(Action.loginUserSuccess, (state, action) => ({
    ...state,
    loggedInUser: action.payload.user
  }));

  builder.addCase(Action.loginUserFail, (state) => ({
    ...state,
    loggedInUser: undefined
  }))
}); 